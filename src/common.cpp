#include "common.hpp"

#include <algorithm>
#include <locale>
#include <string>

using namespace std;

string flame::json_str(string s)
{
    return "\"" + s + "\"";
}

string flame::json_pair(string s1,string s2)
{
    return "\"" + s1 + "\" : \"" + s2 +"\"";
}

string flame::join(string s1,string s2,int flag)
{
    int sz=s1.length();

    switch(flag)
    {
        case ADD_ENTRY_IN_OBJECT:                           // For adding a single entry in object
        {
            s1[sz-1]=',';
            s1+=s2;
            s1+='}';
            break;
        }
        case ADD_EMPTY_ARRAY:                               // For adding a empty array
        {
            s1[sz-1]=',';
            s1+="\"";
            s1+=s2;
            s1+="\":[]}";
            break;
        }
        case ADD_OBJECT_IN_ARRAY:                            // For adding a object in array
        {
            s1[sz-2]=',';
            s1[sz-1]=s2[0];

            s1.append(s2,1,s2.length()-1);

            s1+="]}";
            break;
        }
        case ADD_FIRST_OBJECT_IN_ARRAY:                        // For adding first object in array
        {
            s1[sz-2]=s2[0];
            s1[sz-1]=s2[1];

            s1.append(s2,2,s2.length()-2);

            s1+="]}";
            break;
        }
        case ADD_FIRST_ENTRY_IN_OBJECT:                         // For adding first entry in object
        {   if(s2.size()!=0)
            s1=json_pair(s1,s2);
            return "{" + s1 + "}";
            break;
        }
        case ADD_NESTED_OBJECT:
        {
            s1 = json_str(s1);
            return "{"+  s1+":"+ s2+"}";
            break;
        }
    }
    return s1;
}

string flame::join(list<string> entries, std::function<std::string(std::string, std::string)> join_lines) {
    return join(entries, string(""), join_lines);
}

string flame::join(list<string> entries, string start, std::function<std::string(std::string, std::string)> join_lines) {
    string l= accumulate(entries.begin(), entries.end(), start, join_lines);
    l+=" ]";
    return l;
}

string flame::join(list<bool> entries, std::function<std::string(std::string, std::string)> join_values) {
    return join(entries, string("[ "), join_values);
}

string flame::join(list<int> entries, std::function<std::string(std::string, std::string)> join_values) {
    return join(entries, string("[ "), join_values);
}

string flame::join(list<char> entries, std::function<std::string(std::string, std::string)> join_values) {
    return join(entries, string("[ "), join_values);
}

string flame::join(list<char> entries, string start, std::function<std::string(std::string, std::string)> join_values) {
    std::vector<char> entries_vector{ std::make_move_iterator(std::begin(entries)),std::make_move_iterator(std::end(entries)) };
    list<string> values;
    for(int i=0;i<int(entries.size());i++){
        values.push_back(std::string(1,entries_vector[i]));
    }
    return join(values, start, join_values);
}



string flame::join(list<int> entries, string start, std::function<std::string(std::string, std::string)> join_values) {
    std::vector<int> entries_vector{ std::make_move_iterator(std::begin(entries)),std::make_move_iterator(std::end(entries)) };
    list<string> values;
    for(int i=0;i<int(entries.size());i++){
        values.push_back(std::to_string(entries_vector[i]));
    }
    return join(values, start, join_values);
}


string flame::join(list<bool> entries, string start, std::function<std::string(std::string, std::string)> join_values) {
    function<string(bool)> transform_values = [](bool b) -> string { return (b == 0) ? "false" : "true";};
    list<string> values;
    transform(entries.begin(), entries.end(), back_inserter(values), transform_values);
    return join(values, start, join_values);
}



wstring flame::L(const string &str) {
    wstring ret;
    copy(str.begin(), str.end(), back_inserter(ret));
    return ret;
}

string flame::S(const wstring &str) {
    locale const loc("");
    wchar_t const *from = str.c_str();
    size_t const len = str.size();
    vector<char> buffer(len + 1);
    std::use_facet<std::ctype<wchar_t> >(loc).narrow(from, from + len, '_', &buffer[0]);
    return string(&buffer[0], &buffer[len]);
}
/*
inline string flame::S(const wstring &str) {
    string ret;
    copy(str.begin(), str.end(), back_inserter(ret));
    return ret;
}*/
