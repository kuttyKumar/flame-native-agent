#ifndef JEFF_NATIVE_AGENT_OBJECT_HPP
#define JEFF_NATIVE_AGENT_OBJECT_HPP

#include <jni.h>
#include <jvmti.h>
#include <list>

#include <array>
#include <memory>

#include <boost/noncopyable.hpp>

class Type;

class Object {

public:
    Object();

    Object(const Type *const type, const std::string to_string);

    virtual ~Object();

private:
    const Type *const type;

    const std::string to_string;

public:
    const Type getType() const;

    const std::string toString() const;

    static std::pair<jfieldID*,jint> get_class_fields(jvmtiEnv &jvmti,JNIEnv &jni,jclass klass);

    static std::string iterate_through_fields(jvmtiEnv &jvmti,JNIEnv &jni,jobject object,std::string fieldName);

    static int get_object_array_length(jvmtiEnv &jvmti,JNIEnv &jni,jobject object);

    static std::pair<std::string,std::string> get_field_name_and_signature(jvmtiEnv &jvmti,jclass klass,jfieldID fieldID);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jbooleanArray array, std::string signature);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jintArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jcharArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jshortArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jbyteArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jfloatArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jdoubleArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jlongArray array, std::string signature );

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jobject array, std::string signature,std::string objectName);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jobject object,std::string fieldName);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, bool value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jchar value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jbyte value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jshort value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jint value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jlong value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jfloat value);

    static std::unique_ptr<Object> from(jvmtiEnv &jvmti, JNIEnv &jni, jdouble value);

};

#endif //JEFF_NATIVE_AGENT_OBJECT_HPP
