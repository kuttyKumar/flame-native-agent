# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kumard/flame-native-agent/src/GlobalAgentData.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/GlobalAgentData.cpp.o"
  "/home/kumard/flame-native-agent/src/Object.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/Object.cpp.o"
  "/home/kumard/flame-native-agent/src/Sender.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/Sender.cpp.o"
  "/home/kumard/flame-native-agent/src/StdSender.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/StdSender.cpp.o"
  "/home/kumard/flame-native-agent/src/TcpSender.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/TcpSender.cpp.o"
  "/home/kumard/flame-native-agent/src/Type.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/Type.cpp.o"
  "/home/kumard/flame-native-agent/src/common.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/common.cpp.o"
  "/home/kumard/flame-native-agent/src/jni.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/jni.cpp.o"
  "/home/kumard/flame-native-agent/src/jvmti.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/jvmti.cpp.o"
  "/home/kumard/flame-native-agent/src/main.cpp" "/home/kumard/flame-native-agent/build/CMakeFiles/flame-native-agent.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/lib/jvm/java-8-oracle/include"
  "/usr/lib/jvm/java-8-oracle/include/linux"
  "/usr/lib/jvm/java-7-openjdk-amd64/include"
  "/usr/lib/jvm/java-7-openjdk-amd64/include/linux"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
