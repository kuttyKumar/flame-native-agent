#include <iostream>
#include <cstring>
#include "Object.hpp"

#include "common.hpp"
#include "jni.hpp"
#include "jvmti.hpp"
#include "Type.hpp"

using namespace std;
using namespace boost;
using namespace flame;

Object::Object() : type(nullptr), to_string(string()) {
    // Empty
}

Object::Object(const Type *const type, std::string to_string)
        : type(type), to_string(to_string) {
    // Empty
}

Object::~Object() {
    delete type;
}

const Type Object::getType() const {
    return *type;
}

const string Object::toString() const {
    return to_string;
}

std::pair<std::string,std::string> Object::get_field_name_and_signature(jvmtiEnv &jvmti,jclass klass,jfieldID fieldID){
    char *fieldName,*fieldSignature;
    pair<std::string,std::string> result;
    jvmtiError error = jvmti.GetFieldName(klass,fieldID,&fieldName,&fieldSignature,NULL);
    check_jvmti_error(jvmti,error,"Cannot Get Field Name");
    result.first=string(fieldName);
    result.second=string(fieldSignature);
    deallocate(jvmti,fieldName);
    deallocate(jvmti,fieldSignature);
    return result;
}

std::pair<jfieldID*,jint> Object::get_class_fields(jvmtiEnv &jvmti,JNIEnv &jni,jclass klass){
    jfieldID* field_ptr;
    jint field_count;
    jvmtiError error;
    pair<jfieldID*,jint> fields_pair;
    error = jvmti.GetClassFields(klass,&field_count,&field_ptr);
    check_jvmti_error(jvmti,error,"Cannot Get class fields");
    fields_pair.first=field_ptr;
    fields_pair.second=field_count;
    return fields_pair;
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jbooleanArray array, string signature ){
    list<bool> values = to_list(jni, array);
    string as_string = join(values, [](string a, string b) -> string {

        if(a.compare("[ ")==0)
            return a+b;
        else
            return a + ", " + b; });
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jintArray array, string signature ){
    list<int> values = to_list(jni, array);
    string as_string = join(values,[](string a, string b) -> string {
        if(a.compare("[ ")==0)
            return a+b;
        else
            return a + ", " + b; });
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}


unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jcharArray array, string signature ){
    list<char> values = to_list(jni, array);
    string as_string = join(values, [](string a, string b) -> string { return a + ", " + b; });
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}


unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jshortArray array, string signature ){
    list<short> values = to_list(jni, array);
//    string as_string = join(values, [](short a, short b) -> string { return std::to_string(a) + ", " + std::to_string(b);});
    string as_string = "";
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}


unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jbyteArray array, string signature ){
    list<int> values = to_list(jni, array);
//    string as_string = join(values, [](string a, string b) -> string { return a + ", " + b });
    string as_string = "";
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}



unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jfloatArray array, string signature ){
    list<float> values = to_list(jni, array);
//    string as_string = join(values, [](float a, float b) -> string { return std::to_string(a) + ", " + std::to_string(b); });
    string as_string = "";
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jdoubleArray array, string signature ){
    list<double> values = to_list(jni, array);
//    string as_string = join(values, [](double a, double b) -> string { return std::to_string(a) + ", " + std::to_string(b); });
    string as_string = "";
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jlongArray array, string signature ){
    list<long> values = to_list(jni, array);
//    string as_string = join(values, [](long a, long b) -> string { return std::to_string(a) + ", " + std::to_string(b); });
    string as_string = "";
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    return unique_ptr<Object>(ret);
}

int Object::get_object_array_length(jvmtiEnv &jvmti,JNIEnv &jni,jobject object){
    //cast the object to type jarray
    return jni.GetArrayLength(jarray(object));
}


unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jobject array, string signature,string objectName) {
    int array_len = get_object_array_length(jvmti,jni,array);
    jclass type ;
    string result="[";
    string objSign;
    for(int i=0;i<array_len;i++){

        jobject obj = jni.GetObjectArrayElement(jobjectArray(array),i);
        type = get_object_class(jni, obj);
        objSign = get_class_signature(jvmti, type);
        unique_ptr<Object> ret = from(jvmti, jni, obj,"");
        if(result=="[")
            result+= ret->to_string;
        else
            result+= "," + ret->to_string;

    }
    result+="]";

    Object *ret = new Object(Type::from(jvmti, jni, objSign), result);
    return unique_ptr<Object>(ret);

}


string Object::iterate_through_fields(jvmtiEnv &jvmti,JNIEnv &jni,jobject object,string objName){
    list<string> field_name_filter = {"TRUE","FALSE","serialPersistentFields","CASE_INSENSITIVE_ORDER","serialVersionUID","hash","value","MIN_VALUE","MAX_VALUE","TYPE","digits","DigitTens","DigitOnes","sizeTable","SIZE","BYTES"};
    list<string> field_type_filter = {"int","char","float","bool","double","short","byte","long","String","class java.lang.Integer","class java.lang.Boolean","class java.lang.Character","class java.lang.Float","class java.lang.Double","class java.lang.Long","class java.lang.Short","class java.lang.Byte"};
    jobject classObject = call_method(jni,object,"getClass","()Ljava/lang/Class;");
    jobject fields = call_method(jni,classObject,"getDeclaredFields","()[Ljava/lang/reflect/Field;");
    int field_len = get_object_array_length(jvmti,jni,fields);

    jobject current_field;
    string field_class_signature,return_string = "";
    jobject current_field_value_object;

    for(int i=0;i<field_len;i++){
        jobject result;
        current_field = jni.GetObjectArrayElement(jobjectArray(fields),i);
        result = call_method(jni,current_field,"setAccessible","(Z)V",true);
        jstring field_jstring_name = jstring(call_method(jni,current_field,"getName","()Ljava/lang/String;"));
        const char* field_name = jni.GetStringUTFChars(field_jstring_name,NULL);
        string field_name_string = field_name;
        // this is resulting in core dump needs to be debugged
//        jni.ReleaseStringUTFChars(field_jstring_name,field_name);
        if(std::find(field_name_filter.begin(),field_name_filter.end(),string(field_name))==field_name_filter.end()) {
            jobject field_type = call_method(jni, current_field, "getType", "()Ljava/lang/Class;");
            result = call_method(jni, field_type, "toString", "()Ljava/lang/String;");
            string field_type_class_string = (result == nullptr) ? "" : flame::to_string(jni,
                                                                                         static_cast<jstring>(result));
            current_field_value_object = call_method(jni, current_field, "get",
                                                     "(Ljava/lang/Object;)Ljava/lang/Object;", object);
            if (std::find(field_type_filter.begin(), field_type_filter.end(), field_type_class_string) !=
                field_type_filter.end()) {
                result = call_method(jni, current_field_value_object, "toString", "()Ljava/lang/String;");
                string as_string = (result == nullptr) ? "" : flame::to_string(jni, static_cast<jstring>(result));
                return_string += "\"" + field_name_string + "\":\"" + as_string + "\"" ;
                if(i!=field_len-1)
                    return_string+=",";

            } else {
                return_string += "\"" + field_name_string+"\": {"+iterate_through_fields(jvmti, jni, current_field_value_object, field_name);
            }
        }
    }
    return return_string+" }";
}



unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jobject object,string fieldName) {

    jclass type = get_object_class(jni, object);
    jobject result = call_method(jni, object, "toString", "()Ljava/lang/String;");
    string temp = "",as_string;
    temp+=iterate_through_fields(jvmti,jni,object,fieldName);
    if(fieldName.size()!=0 && temp.compare(" }")!=0) {
        as_string += "{"+temp;
    }else if(fieldName.size()==0 && temp.compare(" }")!=0) {
        as_string += "{"+temp;
    }else {
        as_string = (result == nullptr) ? "" : flame::to_string(jni, static_cast<jstring>(result));
        as_string="\""+as_string+"\"";
    }

    std::string signature = get_class_signature(jvmti, type);
    Object *ret = new Object(Type::from(jvmti, jni, signature), as_string);
    jni.DeleteLocalRef(type);
    return unique_ptr<Object>(ret);
}


unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, bool value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'Z'), (value == 0) ? "false" : "true"));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jchar value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'C'), std::to_string(value)));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jbyte value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'B'), std::to_string(value)));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jshort value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'S'), std::to_string(value)));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jint value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'I'), std::to_string(value)));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jlong value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'J'), std::to_string(value)));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jfloat value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'F'), std::to_string(value)));
}

unique_ptr<Object> Object::from(jvmtiEnv &jvmti, JNIEnv &jni, jdouble value) {
    return unique_ptr<Object>(new Object(Type::from(jvmti, jni, 'D'), std::to_string(value)));
}