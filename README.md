Project for capturing the state of the java virtual machine when an exception occurs.

This Project will be used to build a java agent, which is basically the debugger for the java application.

To Build the project, execute build.sh, this will result in a flame-native-agent file in the build directory.

This agent should then be used as a command line argument when starting the application

Example: java -jar my-custom-app.jar -agentlib:flame-native-agent

Once the application starts, as and when an exception occurs, the state of the variables associated with the
method where the exception occurred is captured, and is posted to another rest API, the source code of which
is not under the same repository, but the API stores all the content in elasticsearch.
