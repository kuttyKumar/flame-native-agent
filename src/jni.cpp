#include "jni.hpp"

#include <boost/assert.hpp>
#include <boost/format.hpp>

#include "GlobalAgentData.hpp"

using namespace std;
using namespace boost;

/**
 * Remember to use jni.DeleteLocalRef
 */
jclass flame::find_class(JNIEnv &jni, string name) {
    jclass type = jni.FindClass(name.c_str());
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to find class");
    return type;
}

/**
 * Remember to use jni.DeleteLocalRef
 */
jclass flame::get_object_class(JNIEnv &jni, jobject object) {
    jclass type = jni.GetObjectClass(object);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get object class");
    return type;
}

jmethodID flame::get_method_id(JNIEnv &jni, jclass type, const string methodName, const string returnSignature) {
    jmethodID methodID = jni.GetMethodID(type, methodName.c_str(), returnSignature.c_str());
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get method ID");
    ASSERT_MSG(methodID != 0, "Expected non-zero method ID");
    return methodID;
}

string flame::call_method(JNIEnv &jni, jobject &object, const string methodName,
                          const string methodSignature, std::function<std::string(jobject)> transformer, ...) {
    jclass type = get_object_class(jni, object);
    jmethodID methodID = get_method_id(jni, type, methodName, methodSignature);
    delete_local_ref(jni, type);

    va_list args;
    jobject result;
    va_start(args, transformer);
    result = call_method(jni, object, methodID, args);
    va_end(args);

    return transformer(result);
}

jmethodID flame::get_static_method_id(JNIEnv &jni,jclass klass,const char *name,const char *sig){
    cout<<"Inside get static method id \n";
    jmethodID  methodID = jni.GetStaticMethodID(klass,name,sig);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get method ID");
    ASSERT_MSG(methodID != 0, "Expected non-zero method ID");
    cout<<"Inside get static method id after assertions\n";
    return methodID;
}

jobject flame::call_static_method(JNIEnv &jni,jclass klass,jmethodID methodID,...){
    va_list  args;
    jobject result;
    va_start(args,methodID);
    result = jni.CallStaticObjectMethodV(klass,methodID,args);
    va_end(args);
    return result;

}

jobject flame::call_static_method(JNIEnv &jni,jobject object,const string methodName,const string methodSignature, ...){
    jclass type = get_object_class(jni, object);
    cout<<"inside call static method\n";
    jmethodID methodID = get_static_method_id(jni, type, methodName.c_str(), methodSignature.c_str());

    va_list  args;
    jobject  result;
    va_start(args,methodSignature);
    result = call_static_method(jni,type,methodID,args);
    va_end(args);
    return result;
}

jobject flame::call_method(JNIEnv &jni,jobject &object,const string methodName,const string methodSignature,jobject argumentObject){
    jclass type = get_object_class(jni, object);
    jmethodID methodID = get_method_id(jni, type, methodName, methodSignature);
    delete_local_ref(jni, type);
    jobject result;
    result = jni.CallObjectMethod(object,methodID,argumentObject);
    return result;

}

jobject flame::call_method(JNIEnv &jni, jobject &object, const string methodName,
                          const string methodSignature, ...) {
    jclass type = get_object_class(jni, object);
    jmethodID methodID = get_method_id(jni, type, methodName, methodSignature);
    delete_local_ref(jni, type);

    va_list args;
    jobject result;
    va_start(args, methodSignature);
    result = call_method(jni, object, methodID, args);
    va_end(args);

    return result;
}

jobject flame::call_method(JNIEnv &jni, jobject &object, jmethodID methodID, ...) {
    va_list args;
    jobject result;
    va_start(args, methodID);
    result = jni.CallObjectMethodV(object, methodID, args);

    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to call method");
    va_end(args);
    return result;
}

string flame::to_string(JNIEnv &jni, jstring str) {
    // Convert to native char array
    const char *chars = jni.GetStringUTFChars(str, JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get string");
    ASSERT_MSG(chars != nullptr, "Expected non-null pointer");

    jsize len = jni.GetStringLength(str);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get string length");

    string ret;
    ret.assign(chars, chars + len);

    jni.ReleaseStringUTFChars(str, chars);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release string");
    return ret;
}

wstring flame::to_wstring(JNIEnv &jni, jstring str) {
    // Convert to native char array
    const jchar *chars = jni.GetStringChars(str, JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get string");
    ASSERT_MSG(chars != nullptr, "Expected non-null pointer");

    jsize len = jni.GetStringLength(str);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get string length");

    wstring ret;
    ret.assign(chars, chars + len);

    jni.ReleaseStringChars(str, chars);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release string");
    return ret;
}

list<bool> flame::to_list(JNIEnv &jni, jbooleanArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jboolean *values = jni.GetBooleanArrayElements(static_cast<jbooleanArray>(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<bool> ret;
    for (jboolean *v = values; v < values + length; v++) {
        bool value = *v > 0;
        ret.push_back(value);
    }
    jni.ReleaseBooleanArrayElements(static_cast<jbooleanArray>(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}

list<int> flame::to_list(JNIEnv &jni, jintArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jint *values = jni.GetIntArrayElements(static_cast<jintArray>(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<int> ret;
    for (jint *v = values; v < values + length; v++) {
        int value = *v ;
        ret.push_back(value);
    }
    jni.ReleaseIntArrayElements(static_cast<jintArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}


list<short> flame::to_list(JNIEnv &jni, jshortArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jshort *values = jni.GetShortArrayElements(static_cast<jshortArray >(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<short> ret;
    for (jshort *v = values; v < values + length; v++) {
        short value = *v ;
        ret.push_back(value);
    }
    jni.ReleaseShortArrayElements(static_cast<jshortArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}

list<int> flame::to_list(JNIEnv &jni, jbyteArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jbyte *values = jni.GetByteArrayElements(static_cast<jbyteArray >(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<int> ret;
    for (jbyte *v = values; v < values + length; v++) {
        int value = *v ;
        ret.push_back(value);
    }
    jni.ReleaseByteArrayElements(static_cast<jbyteArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}


list<float> flame::to_list(JNIEnv &jni, jfloatArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jfloat *values = jni.GetFloatArrayElements(static_cast<jfloatArray >(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<float> ret;
    for (jfloat *v = values; v < values + length; v++) {
        float value = *v ;
        ret.push_back(value);
    }
    jni.ReleaseFloatArrayElements(static_cast<jfloatArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}


list<double> flame::to_list(JNIEnv &jni, jdoubleArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jdouble *values = jni.GetDoubleArrayElements(static_cast<jdoubleArray >(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<double> ret;
    for (jdouble *v = values; v < values + length; v++) {
        double value = *v ;
        ret.push_back(value);
    }
    jni.ReleaseDoubleArrayElements(static_cast<jdoubleArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}

list<long> flame::to_list(JNIEnv &jni, jlongArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jlong *values = jni.GetLongArrayElements(static_cast<jlongArray >(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<long> ret;
    for (jlong *v = values; v < values + length; v++) {
        long value = *v ;
        ret.push_back(value);
    }
    jni.ReleaseLongArrayElements(static_cast<jlongArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}

list<char> flame::to_list(JNIEnv &jni, jcharArray array) {
    jsize length = jni.GetArrayLength(array);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array length");

    jchar *values = jni.GetCharArrayElements(static_cast<jcharArray >(array), JNI_FALSE);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to get array elements");

    list<char> ret;
    for (jchar *v = values; v < values + length; v++) {
        char value = char(*v) ;
        ret.push_back(value);
    }
    jni.ReleaseCharArrayElements(static_cast<jcharArray >(array), values, JNI_ABORT);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release array elements");

    return ret;
}




/*
std::function<std::wstring(jobject)> flame::wstring_transformer(JNIEnv &jni) {
    return [jni](jobject result) mutable {
        return (result == nullptr) ? L"" : flame::to_wstring(jni, static_cast<jstring>(result));
    };
}

std::function<wstring(jobject)> wstring_transformer = [jni](jobject result) mutable {
    return (result == nullptr) ? L"" : flame::to_wstring(*jni, static_cast<jstring>(result));
};
*/

void flame::delete_local_ref(JNIEnv &jni, jclass type) {
    jni.DeleteLocalRef(type);
    ASSERT_MSG(!jni.ExceptionCheck(), "Unable to delete local reference");
}

void flame::throw_by_name(JNIEnv &jni, const string exceptionType, const string exceptionMessage) {
    jclass type = find_class(jni, exceptionType);
    /* if type is NULL, an exception has already been thrown by JVM */
    if (type != NULL) {
        jni.ThrowNew(type, exceptionMessage.c_str());
    }
    jni.DeleteLocalRef(type);
}

/**
 * Use ASSERT_MSG macro.
 *
 * JNI specification 6.1.1:
 * A pending exception raised through the JNI (by calling ThrowNew, for example) does not immediately disrupt
 * the native method execution. JNI programmers must explicitly implement the control flow after an exception has occurred.
 */
void flame::__throw_exception(const char *message, const char *expression, const char *exceptionType,
                             const char *function, const char *file, int line) {
    BOOST_ASSERT_MSG(exceptionType != NULL, "Expected non-null exceptionType");

    auto exceptionMessage = format("JNIException (%s): '%s' %s\n\t%s (%s:%s)")
                            % exceptionType % message
                            % ((expression == NULL) ? "" : (format(" assertion (%s) failed in:") % expression).str())
                            % (function == NULL ? "unknown" : function)
                            % (file == NULL ? "unknown" : file)
                            % line;

    std::cerr << exceptionMessage << std::endl << std::endl;

    JNIEnv *jni = get_current_jni();
    throw_by_name(*jni, exceptionType, exceptionMessage.str());
}

JNIEnv *flame::get_current_jni() {
    JNIEnv *jni;
    gdata.jvm->AttachCurrentThread((void **) &jni, nullptr);  // Get the JNIEnv by attaching to the current thread.
    BOOST_ASSERT_MSG(jni != nullptr, "Unable to attach to current thread to get JNIEnv");
    return jni;
}

/**
 * Use THROW_JAVA_EXCEPTION macro.
 */
void flame::__throw_exception(const char *message, const char *exceptionType,
                             const char *function, const char *file, int line) {
    __throw_exception(message, NULL, exceptionType, function, file, line);
}