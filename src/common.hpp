#ifndef JEFF_NATIVE_AGENT_COMMON_H
#define JEFF_NATIVE_AGENT_COMMON_H

#include <functional>
#include <numeric>
#include <list>
#include <string>

namespace flame {
    const int ADD_FIRST_OBJECT_IN_ARRAY=3;
    const int ADD_EMPTY_ARRAY=1;
    const int ADD_OBJECT_IN_ARRAY=2;
    const int ADD_ENTRY_IN_OBJECT=0;
    const int ADD_FIRST_ENTRY_IN_OBJECT=4;
    const int ADD_NESTED_OBJECT=5;

    std::string json_str(std::string s);

    std::string json_pair(std::string s1, std::string s2);

    std::string join(std::string s1,std::string s2,int flag);

    std::string join(std::list<std::string> entries, std::function<std::string(std::string, std::string)> join_lines);

    std::string join(std::list<std::string> entries, std::string start,
                     std::function<std::string(std::string, std::string)> join_lines);

    std::string join(std::list<bool> entries, std::function<std::string(std::string, std::string)> join_values);

    std::string join(std::list<int> entries, std::function<std::string(std::string, std::string)> join_values);

    std::string join(std::list<char> entries, std::function<std::string(std::string, std::string)> join_values);

    std::string join(std::list<int> entries, std::string start, std::function<std::string(std::string, std::string)> join_values);

    std::string join(std::list<char> entries, std::string start, std::function<std::string(std::string, std::string)> join_values);

    std::string join(std::list<bool> entries, std::string start,
                     std::function<std::string(std::string, std::string)> join_values);

    std::wstring L(const std::string &str);

    std::string S(const std::wstring &str);
}
#endif //JEFF_NATIVE_AGENT_COMMON_H
