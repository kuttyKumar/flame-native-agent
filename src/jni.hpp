#ifndef JEFF_NATIVE_AGENT_JNI_HPP
#define JEFF_NATIVE_AGENT_JNI_HPP

#include <jni.h>

#include <functional>
#include <list>
#include<string>

#include <boost/optional.hpp>

#define THROW_JAVA_EXCEPTION(msg, exception_type) \
    flame::__throw_exception(exception_type, msg, \
        BOOST_CURRENT_FUNCTION, __FILE__, __LINE__);

#define ASSERT_MSG(expr, msg) ((expr) \
    ? ((void)0) \
    : flame::__throw_exception(#expr, msg, AssertionError, \
        BOOST_CURRENT_FUNCTION, __FILE__, __LINE__))

namespace flame {

    constexpr const char *const RuntimeException = "java/lang/RuntimeException";
    constexpr const char *const AssertionError = "java/lang/AssertionError";

    jclass find_class(JNIEnv &jni, std::string name);

    jclass get_object_class(JNIEnv &jni, jobject object);

    jmethodID get_method_id(JNIEnv &jni, jclass type, const std::string methodName,
                            const std::string returnSignature);

    jobject call_static_method(JNIEnv &jni,jobject object,const std::string methodName,const std::string methodSignature, ...);

    jobject call_static_method(JNIEnv &jni,jclass klass,jmethodID methodID,...);

    jmethodID get_static_method_id(JNIEnv &jni,jclass klass,const char *name,const char *sig);

    jobject call_method(JNIEnv &jni,jobject &object,const std::string methodName,const std::string methodSignature,jobject argumentObject);

    std::string call_method(JNIEnv &jni, jobject &object, const std::string methodName,
                              const std::string methodSignature, std::function<std::string(jobject)> transformer, ...);

    jobject call_method(JNIEnv &jni, jobject &object, const std::string methodName,
                              const std::string methodSignature, ...);

    jobject call_method(JNIEnv &jni, jobject &object, jmethodID methodID, ...);

    std::string to_string(JNIEnv &jni, jstring str);

    std::wstring to_wstring(JNIEnv &jni, jstring str);

    std::list<bool> to_list(JNIEnv &jni, jbooleanArray array);

    std::list<int> to_list(JNIEnv &jni, jintArray array);

    std::list<short> to_list(JNIEnv &jni, jshortArray array);

    std::list<int> to_list(JNIEnv &jni, jbyteArray array);

    std::list<float> to_list(JNIEnv &jni, jfloatArray array);

    std::list<double> to_list(JNIEnv &jni, jdoubleArray array);

    std::list<long> to_list(JNIEnv &jni, jlongArray array);

    std::list<char> to_list(JNIEnv &jni, jcharArray array);

    void delete_local_ref(JNIEnv &jni, jclass type);

    JNIEnv *get_current_jni();

    void throw_by_name(JNIEnv &jni, const std::string exceptionType, const std::string exceptionMessage);

    void __throw_exception(const char *message, const char *exceptionType,
                           const char *function, const char *file, int line);

    void __throw_exception(const char *message, const char *expression, const char *exceptionType,
                           const char *function, const char *file, int line);
}

#endif //JEFF_NATIVE_AGENT_JNI_HPP
