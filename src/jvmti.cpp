#include "jvmti.hpp"

#include <sstream>

#include <boost/format.hpp>
#include <boost/uuid/uuid.hpp>            // uuid class
#include <boost/uuid/uuid_generators.hpp> // generators
#include <boost/uuid/uuid_io.hpp>         // streaming operators etc.

#include "common.hpp"
#include "jni.hpp"
#include "GlobalAgentData.hpp"
#include "Object.hpp"
#include "Type.hpp"

using namespace std;
using namespace boost;
using namespace flame;


string flame::get_package_name(string pkg) {
    string package_string = "";
    int index = 0;

    for (unsigned int k = 0; k < pkg.length(); k++) {

        if (pkg[k] == '/') {

            package_string += ".";

        } else if (pkg[k] == ';') {


        } else if (pkg[k] == 'L') {

            index = k;

        } else

            package_string += pkg[k];

    }


    string s = "";
    s.append(package_string, 0, index + 1);
    s.append(package_string, index + 1, package_string.length());
    return s;

}


string flame::get_class_name(string pkg_name) {

    string class_name = "";
    int index = 0;
    bool flag = false;
    for (unsigned int k = 0; k < pkg_name.length(); k++) {

        if (pkg_name[k] == '.'){
            index = k;
            flag=true;
        }



    }
    if(flag){
        string s = "";
        s.append(pkg_name, index + 1, pkg_name.length());
        return s;
    }

    return pkg_name;
}


string flame::get_class_status(jvmtiEnv &jvmti, jclass type) {
    jint status = 0;
    jvmtiError error = jvmti.GetClassStatus(type, &status);
    ASSERT_JVMTI_MSG(error, "Cannot get class status");

    list<string> flags = list<string>();
    if (status & JVMTI_CLASS_STATUS_VERIFIED) {
        flags.push_back("JVMTI_CLASS_STATUS_VERIFIED");
    }
    if (status & JVMTI_CLASS_STATUS_PREPARED) {
        flags.push_back("JVMTI_CLASS_STATUS_PREPARED");
    }
    if (status & JVMTI_CLASS_STATUS_PREPARED) {
        flags.push_back("JVMTI_CLASS_STATUS_INITIALIZED");
    }
    if (status & JVMTI_CLASS_STATUS_PREPARED) {
        flags.push_back("JVMTI_CLASS_STATUS_ERROR");
    }
    if (status & JVMTI_CLASS_STATUS_PREPARED) {
        flags.push_back("JVMTI_CLASS_STATUS_ARRAY");
    }
    if (status & JVMTI_CLASS_STATUS_PREPARED) {
        flags.push_back("JVMTI_CLASS_STATUS_PRIMITIVE");
    }

    auto start = format("%s:") % status;
    auto join_lines = [](string a, string b) { return a + "&" + b; };
    return join(flags, start.str(), join_lines);
}

string flame::get_class_signature(jvmtiEnv &jvmti, jclass type) {
    char *signature;
    jvmtiError error = jvmti.GetClassSignature(type, &signature, nullptr);
    if (error == JVMTI_ERROR_INVALID_CLASS) {
        return "<invalid class>";
    }
    ASSERT_JVMTI_MSG(error, (format("Unable to get class signature, class status: '%s'") %
                             get_class_status(jvmti, type)).str().c_str());

    string ret = string(signature);
    deallocate(jvmti, signature);
    return ret;
}

/* Get a name for a jmethodID */
string flame::get_method_name(jvmtiEnv &jvmti, jmethodID method) {
    jvmtiError error;

    jclass declaringType;
    error = jvmti.GetMethodDeclaringClass(method, &declaringType);
    check_jvmti_error(jvmti, error, "Unable to get method declaring class");

    string type = get_class_signature(jvmti, declaringType);
    if(strstr(type.c_str(),"java")!=NULL||strstr(type.c_str(),"javax")!=NULL||strstr(type.c_str(),"sun")!=NULL){
        return "Class loading Exceptions";
    }else {
        char *name;
        char *sig;
        char *gsig;
        error = jvmti.GetMethodName(method, &name, &sig, &gsig);
        check_jvmti_error(jvmti, error, "Unable to get method name");

        std::stringstream stream;
        stream << type << "#" << name << ((gsig == NULL) ? sig : gsig);
        deallocate(jvmti, gsig);
        deallocate(jvmti, sig);
        deallocate(jvmti, name);

        return stream.str();
    }
}


bool  flame::ignore_invalid_slot_errors(jvmtiError error){
    return error!=JVMTI_ERROR_NONE;

}

unique_ptr<Object> flame::get_local_value(jvmtiEnv &jvmti, JNIEnv &jni, jthread thread, int depth, int slot,
                                          string signature,string fieldName) {
    unsigned long length = signature.length();
    ASSERT_MSG(length >= 0 || length <= 1, (format("Invalid signature: %s") % signature).str().c_str());

    jvmtiError error;
    switch (signature.c_str()[0]) {
        case 'Z': {  /* boolean */
            jint bool_value;
            error = jvmti.GetLocalInt(thread, depth, slot, &bool_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, bool_value > 0);
            else
                return NULL;
        }
        case 'C': {  /* char */
            jint char_value;
            error = jvmti.GetLocalInt(thread, depth, slot, &char_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, char_value);
            else
                return NULL;
        }
        case 'B': {  /* byte */
            jint byte_value;
            error = jvmti.GetLocalInt(thread, depth, slot, &byte_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, byte_value);
            else
                return NULL;
        }
        case 'S': {  /* short */
            jint short_value;
            error = jvmti.GetLocalInt(thread, depth, slot, &short_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, short_value);
            else
                return NULL;
        }
        case 'I': {  /* int */
            jint int_value;
            error = jvmti.GetLocalInt(thread, depth, slot, &int_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, int_value);
            else
                return NULL;
        }
        case 'J': {  /* long */
            jlong long_value;
            error = jvmti.GetLocalLong(thread, depth, slot, &long_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, long_value);
            else
                return NULL;
        }
        case 'F': {  /* float */
            jfloat float_value;
            error = jvmti.GetLocalFloat(thread, depth, slot, &float_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, float_value);
            else
                return NULL;
        }
        case 'D': {  /* double */
            jdouble double_value;
            error = jvmti.GetLocalDouble(thread, depth, slot, &double_value);
            if (!ignore_invalid_slot_errors(error))
                return Object::from(jvmti, jni, double_value);
            else
                return NULL;
        }
        case 'L': {  /* Object */
            jobject object_value;
            error = jvmti.GetLocalObject(thread, depth, slot, &object_value);
            if (!ignore_invalid_slot_errors(error)) {
                auto ret = unique_ptr<Object>();
                if (signature.find("List") != string::npos || signature.find("Set") != string::npos ||
                    signature.find("Vector") != string::npos || signature.find("Stack") != string::npos ||
                    signature.find("Queue") != string::npos || signature.find("Deque") != string::npos ||
                    signature.find("Map") != string::npos) {
                    if (signature.find("Map") != string::npos) {
                        jobject map_key_object = call_method(jni, object_value, "keySet", "()Ljava/util/Set;");
                        jobject map_value_object = call_method(jni, object_value, "values",
                                                               "()Ljava/util/Collection;");
                        map_key_object = call_method(jni, map_key_object, "toArray", "()[Ljava/lang/Object;");
                        map_value_object = call_method(jni, map_value_object, "toArray",
                                                       "()[Ljava/lang/Object;");
                        ret = Object::from(jvmti, jni, map_key_object, "[Ljava/lang/Object;", fieldName);
                        string keys_string = "\"Keys\":" + ret->toString();
                        ret = Object::from(jvmti, jni, map_value_object, "[Ljava/lang/Object;", fieldName);
                        string values_string = "\"Values\":" + ret->toString();
                        Object *map_obj = new Object(Type::from(jvmti, jni, signature),
                                                     "{" + keys_string + "," + values_string + "}");
                        return unique_ptr<Object>(map_obj);

                    } else {
                        object_value = call_method(jni, object_value, "toArray", "()[Ljava/lang/Object;");
                        ret = Object::from(jvmti, jni, object_value, "[Ljava/lang/Object;", fieldName);
                    }

                } else {

                    ret = Object::from(jvmti, jni, object_value, fieldName);
                    jni.DeleteLocalRef(object_value);

                }
                return ret;
            } else
                return NULL;
        }
        case '[': {  /* Array */
            jobject array_value;
            error = jvmti.GetLocalObject(thread, depth, slot, &array_value);
            if (!ignore_invalid_slot_errors(error)) {

                auto ret = unique_ptr<Object>();
//            jint len = jni.GetArrayLength(jarray(array_value));

                switch (signature[1]) {
                    case 'Z': {
                        ret = Object::from(jvmti, jni, (jbooleanArray) array_value, signature);
                        break;
                    }
                    case 'I': {
                        ret = Object::from(jvmti, jni, (jintArray) array_value, signature);
                        break;
                    }
                    case 'S': {
                        ret = Object::from(jvmti, jni, (jshortArray) array_value, signature);
                        break;
                    }
                    case 'D': {
                        ret = Object::from(jvmti, jni, (jdoubleArray) array_value, signature);
                        break;
                    }
                    case 'J': {
                        ret = Object::from(jvmti, jni, (jlongArray) array_value, signature);
                        break;
                    }
                    case 'C': {
                        ret = Object::from(jvmti, jni, (jcharArray) array_value, signature);
                        break;
                    }
                    case 'B': {
                        ret = Object::from(jvmti, jni, (jbyteArray) array_value, signature);
                        break;
                    }
                    case 'F': {
                        ret = Object::from(jvmti, jni, (jfloatArray) array_value, signature);
                        break;
                    }
                    case 'L': {
                        ret = Object::from(jvmti, jni, array_value, signature, fieldName);
                        break;
                    }
                    default: {
                        ret = Object::from(jvmti, jni, jobject(nullptr));
                        cout << "Sorry we were not able to find the data type of that array\n";
                    }
                }

                jni.DeleteLocalRef(array_value);
                ASSERT_MSG(!jni.ExceptionCheck(), "Unable to release local reference");

                return ret;
            }else{
                return NULL;
            }
            default: {
                format msg = format("Not expected signature '%s'") % signature;
                return NULL;
            }
        }
    }
}

list<string> flame::get_method_local_variables(jvmtiEnv &jvmti, JNIEnv &jni, jthread thread, jmethodID method,
                                               int limit, int depth) {
    //removing the usage of limit, use limit if you need to differentiate local variables from parameters
//    jclass thread_class = jni.FindClass("java/lang/Thread");
//    jmethodID getNameId = jni.GetMethodID(thread_class,"getName","()Ljava/lang/String;");
//
//    jstring thread_name_jstring = (jstring)jni.CallObjectMethod(thread,getNameId);
//    string thread_name = jni.GetStringUTFChars(thread_name_jstring,NULL);
//    cout<<"Method Variables of Thread : "<<thread_name<<endl;
//    jni.ReleaseStringUTFChars(thread_name_jstring,NULL);



    jint size;
    jvmtiLocalVariableEntry *entries;
    auto error = jvmti.GetLocalVariableTable(method, &size, &entries);
    if (error == JVMTI_ERROR_ABSENT_INFORMATION) {
        cout<<"Cannot get local variable table information\n";
        return list<string>(0);
    }
    check_jvmti_error(jvmti, error, "Unable to get local variable table");

    list<string> arguments;
    for (int i = 0; i < size;i++) {
        string name = string(entries[i].name);
        string signature = entries[i].signature;
//        cout<<"Variable: "<<name<<" : "<<entries[i].length<<" : "<<entries[i].slot<<" : "<<entries[i].start_location<<endl;
        int slot = entries[i].slot;
        unique_ptr<Object> value = get_local_value(jvmti, jni, thread, depth, slot, signature,name);

        if(value!=NULL) {
            string to_string = value->toString();
            arguments.push_back("\"" + name + "\" : " + to_string );

        }
        deallocate(jvmti, entries[i].generic_signature);
        deallocate(jvmti, entries[i].signature);
        deallocate(jvmti, entries[i].name);
    }

    deallocate(jvmti, entries);

    return arguments;
}

int flame::get_method_arguments_size(jvmtiEnv &jvmti, jmethodID method) {
    jint size;
    auto error = jvmti.GetArgumentsSize(method, &size);
    check_jvmti_error(jvmti, error, "Unable to get arguments size");

    return size;
}

list<string> flame::get_method_arguments(jvmtiEnv &jvmti, JNIEnv &jni, jthread thread, jmethodID method, int depth) {
    list<string> lines;
    int size = get_method_arguments_size(jvmti, method);
    list<string> variables = get_method_local_variables(jvmti, jni, thread, method, size, depth);
//    size = min(size, (int) variables.size());
    size_t variable_size = variables.size();
    for (size_t i = 0; i < variable_size; i++) {
        lines.push_back(variables.front());
        variables.pop_front();
    }
    return lines;
}

/* Get a name for a jthread */
string flame::get_thread_name(jvmtiEnv &jvmti, JNIEnv &jni, jthread thread) {
    jvmtiThreadInfo info = {0};

    /* Get the thread information, which includes the name */
    auto error = jvmti.GetThreadInfo(thread, &info);
    check_jvmti_error(jvmti, error, "Cannot get thread info");

    std::stringstream stream;

    /* The thread might not have a name, be careful here. */
    if (info.name != NULL) {
        stream << info.name;

        /* Every string allocated by JVMTI needs to be freed */
        deallocate(jvmti, (void *) info.name);
    } else {
        stream << "Unknown";
    }

    /* Cleanup JNI references */
    jni.DeleteLocalRef(info.thread_group);
    jni.DeleteLocalRef(info.context_class_loader);

    return stream.str();
}

string flame::get_location(jvmtiEnv &jvmti, jmethodID method, jlocation location) {
    jvmtiJlocationFormat format;

    auto error = jvmti.GetJLocationFormat(&format);
    check_jvmti_error(jvmti, error, "Cannot get location format");

    switch (format) {
        case JVMTI_JLOCATION_JVMBCI:
            return get_bytecode_location(jvmti, method, location);
        case JVMTI_JLOCATION_MACHINEPC:
            return (boost::format("native: %s") % (jlong) location).str();
        case JVMTI_JLOCATION_OTHER:
            return "unknown";
    }
    return "";
}

string flame::get_bytecode_location(jvmtiEnv &jvmti, jmethodID method, jlocation location) {
    jint entryCount;
    jvmtiLineNumberEntry *entries;

    auto error = jvmti.GetLineNumberTable(method, &entryCount, &entries);
    check_jvmti_error(jvmti, error, "Cannot get line number table");

    string ret;

    bool found = false;
    auto entry = entries;
    for (int i = 0; i < entryCount; ++i, entry++) {
        if (entry->start_location == location) {
            ret = (format("line: %s") % entry->line_number).str();
            found = true;
        }
    }
    if (!found) {
        ret = (format("line: %s (~%s)") % entries->line_number % location).str();
    }
    deallocate(jvmti, entries);
    return ret;
}

int flame::get_stack_frame_count(jvmtiEnv &jvmti, jthread thread) {
    jint count_ptr;
    auto error = jvmti.GetFrameCount(thread, &count_ptr);
    check_jvmti_error(jvmti, error, "Unable to get stack frame count");
    return count_ptr;
}

list<string> flame::get_stack_trace(jvmtiEnv &jvmti, JNIEnv &jni, jthread thread,string message,jobject exception) {
    int depth = get_stack_frame_count(jvmti, thread);
    return get_stack_trace(jvmti, jni, thread, depth,message,exception);
}


void flame::get_byte_codes(jvmtiEnv &jvmti,jmethodID methodID,jint *byte_code_count,unsigned char **byte_codes){
    jvmti.GetBytecodes(methodID,byte_code_count,byte_codes);
}

list<string> flame::get_stack_trace(jvmtiEnv &jvmti, JNIEnv &jni, jthread thread, int depth,string message,jobject exception) {

    jobject stack_trace_elements = call_method(jni,exception,"getStackTrace","()[Ljava/lang/StackTraceElement;");
    if(stack_trace_elements==NULL){
        cout<<"Couldn't retrieve stack trace array\n";
    }
    int stack_trace_elements_count = Object::get_object_array_length(jvmti,jni,stack_trace_elements);
    int *line_number_value = new int[stack_trace_elements_count];
    for(int i=0;i<stack_trace_elements_count;i++) {
        jobject obj = jni.GetObjectArrayElement(jobjectArray(stack_trace_elements), i);

        if(obj==NULL){
            cout<<"Couldn't get Object array element\n";
            break;
        }
        jclass stack_trace_element_class = jni.FindClass("java/lang/StackTraceElement");
        jmethodID  get_line_number_methodID = jni.GetMethodID(stack_trace_element_class,"getLineNumber","()I");
        line_number_value[i] = jni.CallIntMethod(obj,get_line_number_methodID);
    }
    jvmtiFrameInfo *frames(new jvmtiFrameInfo[depth]);
    jint count;

    auto error = jvmti.GetStackTrace(thread, 0, depth, frames, &count);
    check_jvmti_error(jvmti, error, "Unable to get stack trace frames");

    jclass exception_class = get_object_class(jni,exception);
    string exception_class_signature = get_class_signature(jvmti,exception_class);
    string exception_package = get_package_name(exception_class_signature);

    list<string> lines;
    const char *flag = "Class loading Exceptions";

    time_t  timev;
    time(&timev);
    long timestamp=timev;

    string json_string=join("Exception_Name",get_class_name(exception_package),ADD_FIRST_ENTRY_IN_OBJECT);
    boost::uuids::uuid uuid = boost::uuids::random_generator()();
    json_string = join(json_string,json_pair("Exception_Id",boost::uuids::to_string(uuid)).c_str(),ADD_ENTRY_IN_OBJECT);

    json_string = join(json_string,json_pair("Timestamp",std::to_string(timestamp)),ADD_ENTRY_IN_OBJECT);

    json_string = join(json_string,"Stack_Trace",ADD_EMPTY_ARRAY);

    int var =1;

    for (size_t i = 0; i < (size_t)count; i++) {
        auto frame = frames[i];
        jvmtiLineNumberEntry *line_num_table;
        jint count4 = 0;
        jvmti.GetLineNumberTable(frame.method, &count4, &line_num_table);
        string method_name = get_method_name(jvmti, frame.method);

        if (strcmp(method_name.c_str(), flag) != 0) {

            list <string> args = get_method_arguments(jvmti, jni, thread, frame.method, i);

            jclass declaringType;

            error = jvmti.GetMethodDeclaringClass(frame.method, &declaringType);
            string package_temp = get_class_signature(jvmti, declaringType);
            string pkg_name = get_package_name(package_temp);
            string class_name = get_class_name(pkg_name);


            // args is containing json strings

            string s = join(args.front(), "", ADD_FIRST_ENTRY_IN_OBJECT);
            args.pop_front();
            list <string> args2 = args;

            for (auto it:args) {
                s = join(s, args2.front(), 0);
                args2.pop_front();
            }

            s = join("Variables", s, ADD_NESTED_OBJECT);

            string temp_str = method_name.substr(method_name.find("#") + 1,
                                                 method_name.find("(") - method_name.find("#") - 1);

            string method_name = temp_str;

            temp_str = json_pair("Method_Name", temp_str);
            s = join(s, temp_str, ADD_ENTRY_IN_OBJECT);

            temp_str = json_pair("Method_Id", std::to_string(var));
            s = join(s, temp_str, ADD_ENTRY_IN_OBJECT);

            temp_str = json_pair("Package_Name", pkg_name);
            s = join(s, temp_str, ADD_ENTRY_IN_OBJECT);

            temp_str = json_pair("Class_Name", class_name);
            s = join(s, temp_str, ADD_ENTRY_IN_OBJECT);

            temp_str = json_pair("Exception_Line_Number", std::to_string(int(frame.location)));
            s = join(s, temp_str, ADD_ENTRY_IN_OBJECT);

            //temp_str=json_pair("Method_Line_Number",std::to_string((int)table_ptr[0].line_number-1));
            //s=join(s,temp_str,ADD_ENTRY_IN_OBJECT);

            var++;

            if (i == 0) {
                json_string = join(json_string, s, ADD_FIRST_OBJECT_IN_ARRAY);
            } else
                json_string = join(json_string, s, ADD_OBJECT_IN_ARRAY);

            auto join_lines = [](string a, string b) { return a + ", " + b; };
            string arguments_values = join(args, join_lines);

            auto line = boost::format("%s%s") % method_name % arguments_values;
            lines.push_back(line.str());

            //  Storing data for method Definition
            method_def_post_data.className = class_name;
            method_def_post_data.packageName = pkg_name;
            method_def_post_data.methodName = method_name;
            method_def_post_data.exceptionId = boost::uuids::to_string(uuid);
            method_def_post_data.exceptionLineNumber = std::to_string(line_number_value[i]);
            method_def_post_data.methodStartLineNumber = std::to_string((int) line_num_table[0].line_number - 1);

            jclass this_class;
            error = jvmti.GetMethodDeclaringClass(frames[i].method, &this_class);
            check_jvmti_error(jvmti, error, "Cannot get method declaring class");

            jclass *classes(new jclass[1]);
            classes[0] = this_class;
            error = jvmti.RetransformClasses(1, classes);
            check_jvmti_error(jvmti, error, "Cannot Retransform Class");

        }
        deallocate(jvmti, line_num_table);
    }
    cout<<json_string<<endl;
    struct curl_slist *list = NULL;
    list = curl_slist_append(list, "Content-Type:text/plain");
    curl_easy_setopt(gdata.curl,CURLOPT_URL,"http://localhost:8080/jvmti-flame/stateObject/addException");
    curl_easy_setopt(gdata.curl,CURLOPT_HTTPHEADER,list);
    curl_easy_setopt(gdata.curl,CURLOPT_POSTFIELDS,json_string.c_str());
    curl_easy_setopt(gdata.curl,CURLOPT_POSTFIELDSIZE,json_string.length());
    curl_easy_perform(gdata.curl);

    curl_slist_free_all(list); /* free the list again */
    return lines;
}

string flame::get_error_name(jvmtiEnv &jvmti, jvmtiError error, const string message) {
    char *error_name = NULL;
    jvmtiError error_ = jvmti.GetErrorName(error, &error_name);
    ASSERT_MSG(error_ == JVMTI_ERROR_NONE, "Another JVMTI ERROR while getting an error name");

    const string messageSeparator = message.empty() ? "" : "; ";
    auto name = format("%s%sJVMTI ERROR: '%d' (%s)") % message % messageSeparator % error %
                (error_name == NULL ? "Unknown" : error_name);
    deallocate(jvmti, error_name);
    return name.str();
}

/* All memory allocated by JVMTI must be freed by the JVMTI Deallocate
 *   interface.
 */
void flame::deallocate(jvmtiEnv &jvmti, void *ptr) {
    jvmtiError error;

    error = jvmti.Deallocate((unsigned char *) ptr);
    check_jvmti_error(jvmti, error, "Cannot deallocate memory");
}

/* Allocation of JVMTI managed memory */
void *flame::allocate(jvmtiEnv &jvmti, jint len) {
    void *ptr;

    jvmtiError error = jvmti.Allocate(len, (unsigned char **) &ptr);
    check_jvmti_error(jvmti, error, "Cannot allocate memory");
    return ptr;
}

void flame::print_possible_capabilities(jvmtiEnv &jvmti) {
    jvmtiError error;

    jvmtiCapabilities potentialCapabilities = {0};
    error = jvmti.GetPotentialCapabilities(&potentialCapabilities);
    check_jvmti_error(jvmti, error, "Unable to get potential JVMTI capabilities.");

    cerr << boost::format("\nPotential JVMTI capabilities:\n"
                                  "can_tag_objects: '%s'\n"
                                  "can_generate_field_modification_events: '%s'\n"
                                  "can_generate_field_access_events: '%s'\n"
                                  "can_get_bytecodes: '%s'\n"
                                  "can_get_synthetic_attribute: '%s'\n"
                                  "can_get_owned_monitor_info: '%s'\n"
                                  "can_get_current_contended_monitor: '%s'\n"
                                  "can_get_monitor_info: '%s'\n"
                                  "can_pop_frame: '%s'\n"
                                  "can_redefine_classes: '%s'\n"
                                  "can_signal_thread: '%s'\n"
                                  "can_get_source_file_name: '%s'\n"
                                  "can_get_line_numbers: '%s'\n"
                                  "can_get_source_debug_extension: '%s'\n"
                                  "can_access_local_variables: '%s'\n"
                                  "can_maintain_original_method_order: '%s'\n"
                                  "can_generate_single_step_events: '%s'\n"
                                  "can_generate_exception_events: '%s'\n"
                                  "can_generate_frame_pop_events: '%s'\n"
                                  "can_generate_breakpoint_events: '%s'\n"
                                  "can_suspend: '%s'\n"
                                  "can_redefine_any_class: '%s'\n"
                                  "can_get_current_thread_cpu_time: '%s'\n"
                                  "can_get_thread_cpu_time: '%s'\n"
                                  "can_generate_method_entry_events: '%s'\n"
                                  "can_generate_method_exit_events: '%s'\n"
                                  "can_generate_all_class_hook_events: '%s'\n"
                                  "can_generate_compiled_method_load_events: '%s'\n"
                                  "can_generate_monitor_events: '%s'\n"
                                  "can_generate_vm_object_alloc_events: '%s'\n"
                                  "can_generate_native_method_bind_events: '%s'\n"
                                  "can_generate_garbage_collection_events: '%s'\n"
                                  "can_generate_object_free_events: '%s'\n"
                                  "can_force_early_return: '%s'\n"
                                  "can_get_owned_monitor_stack_depth_info: '%s'\n"
                                  "can_get_constant_pool: '%s'\n"
                                  "can_set_native_method_prefix: '%s'\n"
                                  "can_retransform_classes: '%s'\n"
                                  "can_retransform_any_class: '%s'\n"
                                  "can_generate_resource_exhaustion_heap_events: '%s'\n"
                                  "can_generate_resource_exhaustion_threads_events: '%s'\n"
    )
            % (potentialCapabilities.can_tag_objects == 1)
            % (potentialCapabilities.can_generate_field_modification_events == 1)
            % (potentialCapabilities.can_generate_field_access_events == 1)
            % (potentialCapabilities.can_get_bytecodes == 1)
            % (potentialCapabilities.can_get_synthetic_attribute == 1)
            % (potentialCapabilities.can_get_owned_monitor_info == 1)
            % (potentialCapabilities.can_get_current_contended_monitor == 1)
            % (potentialCapabilities.can_get_monitor_info == 1)
            % (potentialCapabilities.can_pop_frame == 1)
            % (potentialCapabilities.can_redefine_classes == 1)
            % (potentialCapabilities.can_signal_thread == 1)
            % (potentialCapabilities.can_get_source_file_name == 1)
            % (potentialCapabilities.can_get_line_numbers == 1)
            % (potentialCapabilities.can_get_source_debug_extension == 1)
            % (potentialCapabilities.can_access_local_variables == 1)
            % (potentialCapabilities.can_maintain_original_method_order == 1)
            % (potentialCapabilities.can_generate_single_step_events == 1)
            % (potentialCapabilities.can_generate_exception_events == 1)
            % (potentialCapabilities.can_generate_frame_pop_events == 1)
            % (potentialCapabilities.can_generate_breakpoint_events == 1)
            % (potentialCapabilities.can_suspend == 1)
            % (potentialCapabilities.can_redefine_any_class == 1)
            % (potentialCapabilities.can_get_current_thread_cpu_time == 1)
            % (potentialCapabilities.can_get_thread_cpu_time == 1)
            % (potentialCapabilities.can_generate_method_entry_events == 1)
            % (potentialCapabilities.can_generate_method_exit_events == 1)
            % (potentialCapabilities.can_generate_all_class_hook_events == 1)
            % (potentialCapabilities.can_generate_compiled_method_load_events == 1)
            % (potentialCapabilities.can_generate_monitor_events == 1)
            % (potentialCapabilities.can_generate_vm_object_alloc_events == 1)
            % (potentialCapabilities.can_generate_native_method_bind_events == 1)
            % (potentialCapabilities.can_generate_garbage_collection_events == 1)
            % (potentialCapabilities.can_generate_object_free_events == 1)
            % (potentialCapabilities.can_force_early_return == 1)
            % (potentialCapabilities.can_get_owned_monitor_stack_depth_info == 1)
            % (potentialCapabilities.can_get_constant_pool == 1)
            % (potentialCapabilities.can_set_native_method_prefix == 1)
            % (potentialCapabilities.can_retransform_classes == 1)
            % (potentialCapabilities.can_retransform_any_class == 1)
            % (potentialCapabilities.can_generate_resource_exhaustion_heap_events == 1)
            % (potentialCapabilities.can_generate_resource_exhaustion_threads_events == 1)

         << endl;
}

/* Every JVMTI interface returns an error code, which should be checked
 *   to avoid any cascading errors down the line.
 *   The interface GetErrorName() returns the actual enumeration constant
 *   name, making the error messages much easier to understand.
 */
void flame::check_jvmti_error(jvmtiEnv &jvmti, jvmtiError error, const string message) {
    ASSERT_MSG(error == JVMTI_ERROR_NONE, get_error_name(jvmti, error, message).c_str());
}

bool flame::is_jvmti_error(jvmtiEnv &jvmti, jvmtiError error, const string message) {
    if (error == JVMTI_ERROR_NONE) {
        return false;
    } else {
        std::cerr << "Error: " << get_error_name(jvmti, error, message).c_str() << std::endl;
        return true;
    }
}

/**
 * Use ASSERT_JVMTI_MSG macro.
 */
void ::flame::__throw_jvmti_exception(jvmtiError error, const char *message, const char *exceptionType,
                                      const char *function, const char *file, int line) {
    jvmtiEnv &jvmti = *flame::gdata.jvmti;
    __throw_exception(get_error_name(jvmti, error, message).c_str(), exceptionType, function, file, line);
}
